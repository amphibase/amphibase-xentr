## Process to import XenBase GFF3 file. 

## Prerequisites

* Source: A GFF3 file for XENTR_9.1 from XenBase.
  * URL: http://ftp.xenbase.org/pub/Genomics/JGI/Xentr9.1/XENTR_9.1_Xenbase.gff3
  * Also available at the repository (data/xenTro9_XB2021-04-16.gff3.gz)
  * A timestamp of the file: April, 16, 2021. 

* ID conversion table from NCBI RefSeq.
  * URL: https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/004/195/GCF_000004195.3_Xenopus_tropicalis_v9.1/GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt
  * Also avalable at the repo (data/GCF_000004195.3_Xenopus_tropicalis_v9.1_assembly_report.txt.gz)
  
## Convert 
xenTro9_GCA-JGI.gene.gff3.gz
xenTro9_XB2021-04-16.Errors.gene_list.gz
xenTro9_XB2021-04-16.Errors.gff3.gz
xenTro9_XB2021-04-16.gff3.gz
