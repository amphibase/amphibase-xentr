#!/usr/bin/env python3
import sys
import re

filename_gff3 = sys.argv[1]

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)
filename_err = '%s.Errors.gff3' % filename_base
filename_out = '%s.NoError.gff3' % filename_base

f_err = open(filename_err, 'w')
f_out = open(filename_out, 'w')

dubious_IDs = []

sys.stderr.write('Process %s...\n' % filename_gff3)

f_in = open(filename_gff3, 'r')
for line in f_in:
    if line.startswith('#'):
        f_out.write('%s\n' % line.strip())
        f_err.write('%s\n' % line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    tmp_attr = tokens[-1]
    tmp_id = 'NA'
    tmp_parent_id = 'NA'

    is_dubious = 0
    for tmp in tmp_attr.split(';'):
        if tmp == '':
            continue

        # manual correction for xenTro9
        if tmp.startswith('Xenbase:'):
            # Dbxref=Xenbase:XB-GENE-22062234;Xenbase:XB-GENE-22062234;
            continue

        try:
            (tmp_k, tmp_v) = tmp.split('=')
        except Exception:
            sys.stdout.write("Attr Error: %s" % tmp_attr)

        if tmp_k == 'ID':
            tmp_id = tmp_v
        if tmp_k == 'Parent':
            tmp_parent_id = tmp_v

        if tmp_k == 'erroneousModel' and tmp_v.upper() == 'TRUE':
            is_dubious = 1

        if tmp_k == 'splitModel' and tmp_v.upper() == 'TRUE':
            is_dubious = 1

    if is_dubious > 0:
        dubious_IDs.append(tmp_id)
        if tmp_parent_id in dubious_IDs:
            dubious_IDs.append(tmp_id)

    if tmp_id in dubious_IDs:
        f_err.write(line.strip() + "\n")
    else:
        f_out.write(line.strip() + "\n")

f_in.close()
f_err.close()
f_out.close()
