#!/usr/bin/env python3
import sys
import re

filename_gff3 = sys.argv[1]

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)
filename_out = '%s.clean.gff3' % filename_base
filename_err = '%s.dirty.gff3' % filename_base

f_out = open(filename_out, 'w')
f_err = open(filename_err, 'w')

sys.stderr.write('Process %s...\n' % filename_gff3)

gene_list = dict()
tx_list = dict()
tx2gene = dict()
tx2exon = dict()
tx2cds = dict()

f_in = open(filename_gff3, 'r')
for line in f_in:
    if line.startswith('#'):
        f_out.write('%s\n' % line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    mol_type = tokens[2]
    tmp_attr = tokens[-1]
    
    # Discard the following attributes
    #
    # 24 key: Original          --> needs to be separated
    # 24 key: product           --> needs to be separated
    # 3863 key: provisional     --> incorporated to Name
    # 30885 key: Dbxref         --> needs to be separated
    # 34052 key: Primary        --> not informative
    # 34076 key: Alias          --> not informative
    # 65038 key: gene           --> not informative (similar to Parent)
    # 1284356 key: gbkey        --> redundant to mol_type
    # 43592 key: transcript_id  --> redundant to 'Name'

    # Keep the following attributes
    # 
    # 34052 key: gene_biotype 
    # 77748 key: Name
    # 1258757 key: Parent
    # 1292833 key: ID

    # ref. HUMAN GENCODE r37
    # gene: ID, gene_id, gene_type, gene_name, level
    # transcript: ID, Parent, gene_id, transcript_id, gene_type, gene_name, transcript_type, transcript_name, level
    # exon: ID, Parent(tx), gene_id, transcript_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id 
    # CDS: ID, Parent(tx), gene_id, transcript_id, gene_type, gene_name, transcript_type, transcript_name, transcript_support_level, exon_number, exon_id, protein_id

    # chr19	HAVANA	gene	5993164	6199572	.	-	.	ID=ENSG00000087903.13;gene_id=ENSG00000087903.13;gene_type=protein_coding;gene_name=RFX2;level=1;hgnc_id=HGNC:9983;tag=overlapping_locus;havana_gene=OTTHUMG00000180711.5
    # chr19	HAVANA	transcript	5993164	6110500	.	-	.	ID=ENST00000303657.10;Parent=ENSG00000087903.13;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	exon	6110393	6110500	.	-	.	ID=exon:ENST00000303657.10:1;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=1;exon_id=ENSE00002893133.2;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	exon	6047407	6047504	.	-	.	ID=exon:ENST00000303657.10:2;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=2;exon_id=ENSE00003568552.1;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2
    # chr19	HAVANA	CDS	6047407	6047496	.	-	0	ID=CDS:ENST00000303657.10;Parent=ENST00000303657.10;gene_id=ENSG00000087903.13;transcript_id=ENST00000303657.10;gene_type=protein_coding;gene_name=RFX2;transcript_type=protein_coding;transcript_name=RFX2-201;exon_number=2;exon_id=ENSE00003568552.1;level=2;protein_id=ENSP00000306335.4;transcript_support_level=1;hgnc_id=HGNC:9983;tag=basic,MANE_Select,appris_principal_2,CCDS;ccdsid=CCDS12157.1;havana_gene=OTTHUMG00000180711.5;havana_transcript=OTTHUMT00000452687.2

    tmp_id = 'NA'
    tmp_parent = 'NA'
    tmp_name = 'NotAvail'
    tmp_biotype = 'NA'

    for tmp in tmp_attr.split(';'):
        if tmp == '':
            continue

        try:
            (tmp_k, tmp_v) = tmp.split('=')
        except Exception:
            sys.stdout.write("Attr Error: %s" % tmp_attr)

        if tmp_k == 'ID':
            tmp_id = tmp_v
        if tmp_k == 'Parent':
            tmp_parent = tmp_v
        if tmp_k == 'Name':
            tmp_name = tmp_v
        if tmp_k == 'gene_biotype':
            tmp_biotype= tmp_v
        
    
    if tmp_id == 'NA':
        sys.stderr.write('No ID: %s\n' % tmp_id)
    
    if mol_type == 'gene':
        gene_id = 'ABXETG%07d.1' % (int(tmp_id.replace('gene', '')))
        if gene_id in gene_list:
            sys.stderr.write('Redundant: %s\n' % line.strip())

        gene_name = tmp_name.replace(' ', '')
        if gene_name.find('provisional:') >= 0:
            gene_name = gene_name.split('provisional:')[1].replace(']', '')
        
        tmp_attr = 'ID=%s;Name=%s;gene_id=%s;gene_name=%s;gene_type=%s;level=3' % (gene_id, gene_name, gene_id, gene_name, tmp_biotype)
        tmp_line = '\t'.join([seq_id, 'AmphiBase', 'gene', tokens[4], tokens[5], tokens[6], tokens[7], tmp_attr])
        f_out.write('%s\n' % tmp_line)
        gene_list[gene_id] = {'name': tmp_name, 'tx_list': [], 'line': tmp_line}

    elif mol_type == 'mRNA':
        tx_id = 'ABXETT%07d.1' % (int(tmp_id.replace('rna', '')))
        gene_id = 'ABXETG%07d.1' % (int(tmp_parent.replace('gene', '')))
        if tx_id not in tx2gene:
            tx2gene[tx_id] = gene_id

        tmp_attr = 'ID=%s;Parent=%s;gene_id=%s;gene_name=%s;transcript_id=%s;gene_type=%s;level=3' % (tx_id, gene_id, gene_id, gene_name, tx_id, tmp_biotype)
        tmp_line = '\t'.join([seq_id, 'AmphiBase', 'transcript', tokens[4], tokens[5], tokens[6], tokens[7], tmp_attr])

        if gene_id not in gene_list:
            f_err.write('%s\n' % tmp_line)
        else:
            f_out.write('%s\n' % tmp_line)
            gene_list[gene_id]['tx_list'].append(tx_id)

    elif mol_type == 'exon':
        # manual correction
        # if tmp_parent == 'gene2763':
        #     tmp_parent = 'rna9130'

        if tmp_parent.startswith('gene'):
            print(line.strip())
            continue

        tx_id = 'ABXETT%07d.1' % (int(tmp_parent.replace('rna', '')))
        if tx_id not in tx2gene:
            print(tx_id, line.strip())
            continue

        gene_id = tx2gene[tx_id]
        if tx_id not in tx2exon:
            tx2exon[tx_id] = 1
        else:
            tx2exon[tx_id] += 1

        exon_number = tx2exon[tx_id]
        exon_id = 'exon:%s:%d' % (tx_id, exon_number)

        tmp_attr = 'ID=%s;Parent=%s;gene_id=%s;gene_name=%s;transcript_id=%s;exon_id=%s;exon_number=%d;gene_type=%s;level=3' %\
                   (exon_id, tx_id, gene_id, gene_name, tx_id, exon_id, exon_number, tmp_biotype)
        tmp_line = '\t'.join([seq_id, 'AmphiBase', 'exon', tokens[4], tokens[5], tokens[6], tokens[7], tmp_attr])
    #elif mol_type == 'CDS':
f_in.close()

f_out.close()
f_err.close()
