#!/usr/bin/env python3
import sys
import re

filename_gff3 = 'xenTro9_XB2021-04-16.ucsc.gff3'

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)
filename_out = '%s.id_list.txt' % filename_base
filename_err = '%s.id_err.txt' % filename_base

sys.stderr.write('Process %s...\n' % filename_gff3)

gene2tx = dict()
tx2gene = dict()
tx2exon = dict()
tx2cds = dict()

err_gene_list = []
err_tx_list = []

f_in = open(filename_gff3, 'r')
for line in f_in:
    if line.startswith('#'):
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    mol_type = tokens[2]
    tmp_attr = tokens[-1]

    # manual fix for a typo
    if tmp_attr.startswith('ID=gene11611;'):
        tmp_attr = tmp_attr.replace(';Xenbase:XB-GENE-22062234', '')
    
    tmp_id = 'NA'
    tmp_parent = 'NA'
    tmp_name = 'NotAvail'
    tmp_biotype = 'NA'

    for tmp in tmp_attr.split(';'):
        if tmp == '':
            continue

        try:
            (tmp_k, tmp_v) = tmp.split('=')

        except Exception:
            sys.stdout.write("Attr Error: %s" % tmp_attr)

        if tmp_k == 'ID':
            tmp_id = tmp_v
        if tmp_k == 'Parent':
            tmp_parent = tmp_v
        if tmp_k == 'Name':
            tmp_name = tmp_v.lower()
        if tmp_k == 'gene_biotype':
            tmp_biotype= tmp_v
    
    if tmp_id == 'NA':
        sys.stderr.write('No ID: %s\n' % tmp_id)
    
    if mol_type in ['gene', 'tRNA', 'rRNA']:
        gene2tx[tmp_id] = []

    elif mol_type in ['mRNA', 'lnc_RNA', 'primary_transcript', 'transcript', 'miRNA']:
        tx2exon[tmp_id] = []
        tx2cds[tmp_id] = []
        tx2gene[tmp_id] = tmp_parent
        if tmp_parent not in gene2tx:
            gene2tx[tmp_parent] = []
        gene2tx[tmp_parent].append(tmp_id)

    elif mol_type == 'exon':
        # manual fix 
        if tmp_parent == 'gene2763':
            tmp_parent = 'rna9130'

        if tmp_parent.startswith('rna'):
            if tmp_parent in tx2exon:
                tx2exon[tmp_parent].append(tmp_id)
            else:
                err_tx_list.append("%s;no_tx_id" % tmp_parent)
        else:
            err_gene_list.append("%s;no_tx_id" % tmp_parent)

    elif mol_type == 'CDS':
        if tmp_parent.startswith('rna'):
            if tmp_parent in tx2cds:
                tx2cds[tmp_parent].append(tmp_id)
            else:
                err_tx_list.append(tmp_parent)
        else:
            err_gene_list.append(tmp_parent)
    else:
        print(line.strip())
f_in.close()

f_out = open(filename_out, 'w')
f_err = open(filename_err, 'w')
for tmp_gene, tmp_tx_list in gene2tx.items():
    if tmp_gene in err_gene_list:
        f_err.write('Gene\t%s\n' % tmp_gene)
    else:
        for tmp_tx in tmp_tx_list:
            if tmp_tx in err_tx_list:
                f_err.write('Tx\t%s\n' % tmp_tx)
            else:
                for tmp_e in tx2exon[tmp_tx]:
                    f_out.write("Exon\t%s\t%s\t%s\n" % (tmp_gene, tmp_tx, tmp_e))
                for tmp_cds in tx2cds[tmp_tx]:
                    f_out.write("CDS\t%s\t%s\t%s\n" % (tmp_gene, tmp_tx, tmp_cds))
f_out.close()
f_err.close()
