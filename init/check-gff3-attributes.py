#!/usr/bin/env python3
import gzip
import sys
import re

filename_gff3 = sys.argv[1]

filename_base = re.sub(r'\.gff(.)*$', '', filename_gff3)
filename_out = '%s.NoAliasGbkey.gff3' % filename_base

f_out = open(filename_out, 'w')

sys.stderr.write('Process %s...\n' % filename_gff3)

f_in = open(filename_gff3, 'r')
if filename_gff3.endswith('.gz'):
    f_in = gzip.open(filename_gff3, 'rt')

for line in f_in:
    if line.startswith('#'):
        f_out.write('%s\n' % line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    mol_type = tokens[2]
    tmp_attr = tokens[-1]
    tmp_id = 'NA'
    tmp_parent_id = 'NA'

    is_dubious = 0
    for tmp in tmp_attr.split(';'):
        if tmp == '':
            continue

        ## manual correction for xenTro9
        if tmp.startswith('Xenbase:'):
            # Dbxref=Xenbase:XB-GENE-22062234;Xenbase:XB-GENE-22062234;
            continue

        try:
            (tmp_k, tmp_v) = tmp.split('=')
        except Exception:
            sys.stdout.write("Attr Error: %s" % tmp_attr)

        print("key:", tmp_k)
        if tmp_k == 'gbkey':
            print('gbkey=%s, mol_type=%s' % (tmp_v, mol_type))
        if tmp_k == 'Primary':
            print('Primary=%s, mol_type=%s' % (tmp_v, mol_type))
    
    
f_in.close()
f_out.close()
